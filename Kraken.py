import Crypto as crypto
import requests
import json

class Kraken(crypto.CryptoExchange):

    public_api_url = 'https://api.kraken.com/0/public/'

    currency_mapping = {
        'btc' : 'xxbtzusd',
        'eth' : 'XETHZUSD',
        'xrp' : 'XXRPZUSD',
        'eos' : 'EOSUSD',
        'ltc' : 'XLTCZUSD',
        'bch' : 'BCHUSD'
    }

    def __init__(self, APIkey, APIsecret, currencies):
        super().__init__(APIkey, APIsecret, currencies)

    def supported_currency(self, currency):
        if currency.lower() in self.currencies or currency.upper() in self.currencies:
            return True
        else:
            return False

    def ticker(self, currency):
        if self.supported_currency(currency):
            params = {'pair': self.currency_mapping[currency.lower()].lower()}
            response = requests.get(self.public_api_url + 'Ticker', params)
            if response.ok:
                json_data = {}
                data = json.loads(response.content)
                btc_data = data['result']
                stats = btc_data[self.currency_mapping[currency.lower()].upper()]
                for key in stats:
                    if key == 'a':
                        json_data['ask'] = stats[key][0]
                    elif key == 'b':
                        json_data['bid'] = stats[key][0]
                    elif key == 'c':
                        json_data['last'] = stats[key][0]
                    elif key == 'v':
                        json_data['volume'] = stats[key][0]
                    elif key == 'l':
                        json_data['low'] = stats[key][1]
                    elif key == 'h':
                        json_data['high'] = stats[key][1]
                    elif key == 'o':
                        json_data['open'] = stats[key]      
                return json_data             
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}

    def order_book(self, currency):
        if self.supported_currency(currency):
            params = {
                'pair':self.currency_mapping[currency.lower()].lower(),
                'count':10
                }
            response = requests.get(self.public_api_url + 'Depth', params)
            if response.ok:
                json_data = {}
                data = json.loads(response.content)
                btc_data = data['result']
                ask_bid = btc_data[self.currency_mapping[currency.lower()].upper()]
                for key in ask_bid:
                    json_data[key] = ask_bid[key]
                    '''
                    print(key + ":")
                    for order in ask_bid[key]:
                        print(order)
                    '''
                return json_data
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}

    def trades(self, currency):
        if self.supported_currency(currency):
            params = {'pair':self.currency_mapping[currency.lower()].lower()}
            response = requests.get(self.public_api_url + 'Trades', params)
            if response.ok:
                data = json.loads(response.content)
                btc_data = data['result']
                trades = btc_data[self.currency_mapping[currency.lower()].upper()]
                json_data = {}
                id = 0
                for trade in trades:
                    json_data[id] = trade
                    id += 1
                    if id == 10:
                        return json_data
                return json_data
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}
    
    #TODO
    def buy(self, currency, rate, amount):
        pass

    #TODO
    def sell(self, currency, rate, amount):
        pass


