import Poloneix as poloneix_exchange
from flask import Flask, jsonify, request
# Creating web server
app = Flask(__name__)

supported_currencies = ['BTC', 'XRP', 'ETH', 'EOS', 'LTC', 'BCH']
#Poloniex instance
poloniex = poloneix_exchange.Poloniex(
    b'RIQLNWYL-9JAW7LED-RERIXTCG-GMEZILF0',
    b'605047018c0df5e8522879542470c88b2f5ebe7a1faa43eb74f983c98ca23d5fa294324b71f073196572d91c0b7fe414cbf04b01ca8675e5a9956c6bbd125eab', 
    supported_currencies
    )


# ticker from Poloniex 
@app.route('/poloniex/ticker', methods = ['GET'])
def poloneix_ticker():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloniex.ticker(currency)
    return jsonify(data), 200

# orders from Poloniex
@app.route('/poloniex/order_book', methods = ['GET'])
def poloneix_order_book():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloniex.order_book(currency)
    return jsonify(data), 200

# lates trades performed on Poloniex exchange
@app.route('/poloniex/trades', methods = ['GET'])
def poloneix_trades():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloniex.trades(currency)
    return jsonify(data), 200

# open websocket for Poloniex 
@app.route('/poloniex/start_streaming', methods = ['GET'])
def poloneix_start_streaming():
    return jsonify(poloniex.start_streaming_ticker()), 200

# open websocket for Poloniex 
@app.route('/poloniex/stop_streaming', methods = ['GET'])
def poloneix_stop_streaming():
    return jsonify(poloniex.stop_streaming_ticker()), 200

# open websocket for Poloniex 
@app.route('/poloniex/last_price', methods = ['GET'])
def poloneix_last_price():
    return jsonify(poloniex.read_last_price()), 200

# balance of selected crypto currency 
@app.route('/poloniex/balance', methods = ['GET'])
def poloneix_balance():
    if poloniex.APIkey is None or poloniex.APISecret is None:
        return jsonify({'msg':'API key/Secret key is missing.'}), 400
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloniex.returnBalance(currency)
    return jsonify(data), 200

# wallet address for selected crypto curerncy 
@app.route('/poloniex/wallet_address', methods = ['GET'])
def poloneix_wallet_address():
    if poloniex.APIkey is None or poloniex.APISecret is None:
        return jsonify({'msg':'API key/Secret key is missing.'}), 400
    data = poloniex.returnDepositAddresses()
    return jsonify(data), 200
    
# open orders(not yoet executed) for selected crypto currency
@app.route('/poloniex/open_orders', methods = ['GET'])
def poloneix_open_orders():
    if poloniex.APIkey is None or poloniex.APISecret is None:
        return jsonify({'msg':'API key/Secret key is missing.'}), 400
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloniex.returnOpenOrders(currency)
    return jsonify(data), 200

# your trades for given crypto currency
@app.route('/poloniex/trade_history', methods = ['GET'])
def poloneix_trade_history():
    if poloniex.APIkey is None or poloniex.APISecret is None:
        return jsonify({'msg':'API key/Secret key is missing.'}), 400
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloniex.returnTradeHistory(currency)
    return jsonify(data), 200

# buy crypto curency for usd:
# currency
# limit price
# amount
@app.route('/poloniex/buy', methods = ['POST'])
def poloneix_buy():
    if poloniex.APIkey is None or poloniex.APISecret is None:
        return jsonify({'msg':'API key/Secret key is missing.'}), 400
    currency = request.values.get('currency')
    price = request.values.get('price')
    amount = request.values.get('amount')
    if currency is None or price is None or amount is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloniex.buy(currency, price, amount)
    return jsonify(data), 200

# sell crypto curency for usd:
# currency
# limit price
# amount
@app.route('/poloniex/sell', methods = ['POST'])
def poloneix_sell():
    if poloniex.APIkey is None or poloniex.APISecret is None:
        return jsonify({'msg':'API key/Secret key is missing.'}), 400
    currency = request.values.get('currency')
    price = request.values.get('price')
    amount = request.values.get('amount')
    if currency is None or price is None or amount is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloniex.sell(currency, price, amount)
    return jsonify(data), 200

# Starting the server
app.run(host = '0.0.0.0', port = 5003)