import Bitstamp as bitstamp_exchange
from flask import Flask, jsonify, request
# Creating web server
app = Flask(__name__)

supported_currencies = ['BTC']
#Bitstamp
bitstamp = bitstamp_exchange.Bitstamp('','', supported_currencies)

# ticker from Bitstamp 
@app.route('/bitstamp/ticker', methods = ['GET'])
def bitstamp_ticker():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = bitstamp.ticker(currency)
    return jsonify(data), 200

# orders from Bitstamp 
@app.route('/bitstamp/order_book', methods = ['GET'])
def bitstamp_order_book():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = bitstamp.order_book(currency)
    return jsonify(data), 200

# trades from Bitstamp 
@app.route('/bitstamp/trades', methods = ['GET'])
def bitstamp_trades():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = bitstamp.trades(currency)
    return jsonify(data), 200


# Starting the server
app.run(host = '0.0.0.0', port = 5002)