from abc import ABC, abstractmethod

#interface for interacting with crypto exchanges
class CryptoExchange(ABC):

    #api key, secret for signing token, list of supported currencies
    def __init__(self, APIkey, APIsecret, currencies):
        self.APIkey = APIkey
        self.APISecret = APIsecret
        self.currencies = currencies
        super().__init__()

    #ticker data for given currency
    @abstractmethod
    def ticker(self, currency):
        pass

    #order book for given currency
    @abstractmethod
    def order_book(self, currency):
        pass

    #trades for given currency
    @abstractmethod
    def trades(self, currency):
        pass

    #buy cryptocurrency
    @abstractmethod
    def buy(self, currency, rate, amount):
        pass

    #sell cryptocurrency
    @abstractmethod
    def sell(self, currency, rate, amount):
        pass

    