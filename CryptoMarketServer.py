import Kraken as kraken_exchange
import Poloneix as poloneix_exchange
import Bitstamp as bitstamp_exchange
from flask import Flask, jsonify, request
# Creating web server
app = Flask(__name__)

supported_currencies = ['BTC']
#Bitstamp
bitstamp = bitstamp_exchange.Bitstamp('','', supported_currencies)
#Kraken
kraken = kraken_exchange.Kraken('','', supported_currencies)
#Poloneix
poloneix = poloneix_exchange.Poloneix('','', supported_currencies)


# ticker from Bitstamp 
@app.route('/bitstamp/ticker', methods = ['GET'])
def bitstamp_ticker():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = bitstamp.ticker(currency)
    return jsonify(data), 200

# orders from Bitstamp 
@app.route('/bitstamp/order_book', methods = ['GET'])
def bitstamp_order_book():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = bitstamp.order_book(currency)
    return jsonify(data), 200

# trades from Bitstamp 
@app.route('/bitstamp/trades', methods = ['GET'])
def bitstamp_trades():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = bitstamp.trades(currency)
    return jsonify(data), 200

# ticker from Kraken 
@app.route('/kraken/ticker', methods = ['GET'])
def kraken_ticker():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = kraken.ticker(currency)
    return jsonify(data), 200

# orders from Kraken 
@app.route('/kraken/order_book', methods = ['GET'])
def kraken_order_book():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = kraken.order_book(currency)
    return jsonify(data), 200

# trades from Kraken 
@app.route('/kraken/trades', methods = ['GET'])
def kraken_trades():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = kraken.trades(currency)
    return jsonify(data), 200


# ticker from Poloneix 
@app.route('/poloneix/ticker', methods = ['GET'])
def poloneix_ticker():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloneix.ticker(currency)
    return jsonify(data), 200

# orders from Poloneix
@app.route('/poloneix/order_book', methods = ['GET'])
def poloneix_order_book():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloneix.order_book(currency)
    return jsonify(data), 200

# trades from Poloneix 
@app.route('/poloneix/trades', methods = ['GET'])
def poloneix_trades():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = poloneix.trades(currency)
    return jsonify(data), 200

# open websocket for Poloneix 
@app.route('/poloneix/start_streaming', methods = ['GET'])
def poloneix_start_streaming():
    return jsonify(poloneix.start_streaming_ticker()), 200

# open websocket for Poloneix 
@app.route('/poloneix/stop_streaming', methods = ['GET'])
def poloneix_stop_streaming():
    return jsonify(poloneix.stop_streaming_ticker()), 200

# open websocket for Poloneix 
@app.route('/poloneix/last_price', methods = ['GET'])
def poloneix_last_price():
    return jsonify(poloneix.read_last_price()), 200


# Starting the server
app.run(host = '0.0.0.0', port = 5000)