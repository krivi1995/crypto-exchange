import Kraken as kraken_exchange
from flask import Flask, jsonify, request
# Creating web server
app = Flask(__name__)

supported_currencies = ['BTC', 'XRP', 'ETH', 'EOS', 'LTC', 'BCH']
#Kraken instance
kraken = kraken_exchange.Kraken('','', supported_currencies)


# ticker from Kraken 
@app.route('/kraken/ticker', methods = ['GET'])
def kraken_ticker():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = kraken.ticker(currency)
    return jsonify(data), 200

# orders from Kraken 
@app.route('/kraken/order_book', methods = ['GET'])
def kraken_order_book():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = kraken.order_book(currency)
    return jsonify(data), 200

# trades from Kraken 
@app.route('/kraken/trades', methods = ['GET'])
def kraken_trades():
    currency = request.args.get('currency')
    if currency is None:
        return jsonify({'msg':'Currency argument is missing.'}), 400
    data = kraken.trades(currency)
    return jsonify(data), 200


# Starting the server
app.run(host = '0.0.0.0', port = 5001)