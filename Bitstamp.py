import Crypto as crypto
import requests
import json

class Bitstamp(crypto.CryptoExchange):

    def __init__(self, APIkey, APIsecret, currencies):
        super().__init__(APIkey, APIsecret, currencies)

    def supported_currency(self, currency):
        if currency.lower() in self.currencies or currency.upper() in self.currencies:
            return True
        else:
            return False

    def ticker(self, currency):
        if self.supported_currency(currency):
            response = requests.get('https://www.bitstamp.net/api/v2/ticker_hour/btcusd')
            if response.ok:
                json_data = {}
                data = json.loads(response.content)
                for key in data:
                    json_data[key] = data[key]
                return json_data
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}

    def order_book(self, currency):
        if self.supported_currency(currency):
            response = requests.get('https://www.bitstamp.net/api/v2/order_book/btcusd/')
            if response.ok:
                json_data = {}
                data = json.loads(response.content)
                for key in data:
                    json_data[key] = data[key]
                return json_data                    
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}

    def trades(self, currency):
        if self.supported_currency(currency):
            response = requests.get('https://www.bitstamp.net/api/transactions/')
            if response.ok:
                id = 0
                json_data = {}
                data = json.loads(response.content)
                for trade in data:
                    json_data[id] = trade
                    id += 1
                    if id == 10:
                        return json_data
                    #for key in trade:
                    #    print(key + ":" + str(trade[key]))
                return json_data
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}


    #TODO
    def buy(self, currency, rate, amount):
        pass

    #TODO
    def sell(self, currency, rate, amount):
        pass
 
