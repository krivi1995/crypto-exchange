import Crypto as crypto
import requests
import json
import websocket
import threading
import time
import hmac, hashlib
import urllib.parse


#shared variable for storing price update from web socket
#websocket is used for live streaming prices for BTC
last_price = {}
#lock for critical section (reading, writing) for last_price
lock = threading.Lock()

#Poloniex class is wrapper for exchange API 
class Poloniex(crypto.CryptoExchange):

    public_api_url = 'https://poloniex.com/public'  
    trading_api_url = 'https://poloniex.com/tradingApi'

    #request parameter : exchange api currency pair
    currency_mapping = {
        'btc' : 'USDT_BTC',
        'eth' : 'USDT_ETH',
        'xrp' : 'USDT_XRP',
        'eos' : 'USDT_EOS',
        'ltc' : 'USDT_LTC',
        'bch' : 'USDT_BCH'
    }

    def __init__(self, APIkey, APIsecret, currencies):
        super().__init__(APIkey, APIsecret, currencies)
        self.ws = None          #websocket for live ticker data
        self.ws_thread = None   #thread for streaming data over websocket
        self.streaming = False  #flag if socket for streaming is open

    #-----PUBLIC API-----#

    def supported_currency(self, currency):
        if currency.lower() in self.currencies or currency.upper() in self.currencies:
            return True
        else:
            return False

    def ticker(self, currency):
        if self.supported_currency(currency):
            params = {
                'command':'returnTicker'
                }
            response = requests.get(self.public_api_url, params)
            if response.ok:
                json_data = {}
                data = json.loads(response.content)[self.currency_mapping[currency.lower()]]
                for key in data:
                    if key == 'lowestAsk':
                        json_data['ask'] = data[key]
                    elif key == 'highestBid':
                        json_data['bid'] = data[key]
                    elif key == 'last':
                        json_data['last'] = data[key]
                    elif key == 'baseVolume':
                        json_data['volume'] = data[key]
                    elif key == 'low24hr':
                        json_data['low'] = data[key]
                    elif key == 'high24hr':
                        json_data['high'] = data[key]
                    elif key == 'o':
                        json_data['open'] = ''     
                return json_data 
            else:
                return {'msg': 'Cannont get data from server'}                   
        else:
            return {'msg':'Unsupported currency.'}

    def order_book(self, currency):
        if self.supported_currency(currency):
            params = {
                'command':'returnOrderBook',
                'currencyPair': self.currency_mapping[currency.lower()],
                'depth':'10'
                }
            response = requests.get(self.public_api_url, params)
            if response.ok:
                json_data = {}
                ask_bid = json.loads(response.content)
                for key in ask_bid:
                    json_data[key] = ask_bid[key]
                return json_data
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}

    def trades(self, currency):
        if self.supported_currency(currency):
            params = {
                'command':'returnTradeHistory',
                'currencyPair':self.currency_mapping[currency.lower()]
                }
            response = requests.get(self.public_api_url, params)
            if response.ok:
                json_data = {}
                id = 0
                trades = json.loads(response.content)
                for trade in trades:
                    json_data[id] = trade
                    id += 1
                    if id==10:
                        return json_data
                return json_data
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}
    
    #strarts receiving live ticker updates over web socket
    #seperate thread
    def start_streaming_ticker(self):
        if not self.streaming:
            self.streaming = True
            websocket.enableTrace(True) 
            self.ws = websocket.WebSocketApp("wss://api2.poloniex.com/",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)                  
            self.ws.on_open = on_open
            self.ws.keep_running = True 
            self.ws_thread = threading.Thread(target=self.ws.run_forever)
            self.ws_thread.daemon = True
            self.ws_thread.start()
            return {'msg':'Websocket is opened.'}
        else:
            return {'msg':'Websocket is already open'}
    
    #stops receiving live ticker updates over web socket
    def stop_streaming_ticker(self):
        if self.streaming:
            self.streaming = False
            self.ws.keep_running = False
            return {'msg':'Websocket is closed.'}
        else:
            return {'msg':'Websocket is not open. Cannot close it.'}
    
    def read_last_price(self):
        global last_price
        global lock
        lock.acquire()
        price = last_price
        lock.release()
        return price

    #-----PRIVATE TRADING API-----#

    def returnBalance(self, currency):
        if self.supported_currency(currency):
            params = {}
            params['command'] = 'returnBalances'
            params['nonce'] = int(time.time()*1000) 
            post_data = urllib.parse.urlencode(params).encode('utf8')
            signed_data = hmac.new(self.APISecret, post_data, hashlib.sha512).hexdigest()
            headers = {
                    'Key': self.APIkey,
                    'Sign': signed_data,
                    'Content-Type': 'application/x-www-form-urlencoded'
            }
            response = requests.post(self.trading_api_url, headers = headers, data = post_data)
            if response.ok:
                currency_ballance = {}
                balances = json.loads(response.content)
                currency_ballance[currency.upper()] = balances[currency.upper()]
                return currency_ballance
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}    

    def returnDepositAddresses(self):
        params = {}
        params['command'] = 'returnDepositAddresses'
        params['nonce'] = int(time.time()*1000) 
        post_data = urllib.parse.urlencode(params).encode('utf8')
        signed_data = hmac.new(self.APISecret, post_data, hashlib.sha512).hexdigest()
        headers = {
                'Key': self.APIkey,
                'Sign': signed_data,
                'Content-Type': 'application/x-www-form-urlencoded'
        }
        response = requests.post(self.trading_api_url, headers = headers, data = post_data)
        if response.ok:
            addresses = json.loads(response.content)
            return addresses
        else:
            return {'msg': 'Cannont get data from server'}

    def returnOpenOrders(self, currency):
        if self.supported_currency(currency):
            params = {}
            params['command'] = 'returnOpenOrders'
            params['nonce'] = int(time.time()*1000) 
            params['currencyPair'] = self.currency_mapping[currency.lower()]
            post_data = urllib.parse.urlencode(params).encode('utf8')
            signed_data = hmac.new(self.APISecret, post_data, hashlib.sha512).hexdigest()
            headers = {
                    'Key': self.APIkey,
                    'Sign': signed_data,
                    'Content-Type': 'application/x-www-form-urlencoded'
            }
            response = requests.post(self.trading_api_url, headers = headers, data = post_data)
            if response.ok:
                orders = json.loads(response.content)
                return orders
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}
    

    def returnTradeHistory(self, currency):
        if self.supported_currency(currency):
            params = {}
            params['command'] = 'returnTradeHistory'
            params['nonce'] = int(time.time()*1000)
            params['currencyPair'] = 'all' 
            post_data = urllib.parse.urlencode(params).encode('utf8')
            signed_data = hmac.new(self.APISecret, post_data, hashlib.sha512).hexdigest()
            headers = {
                    'Key': self.APIkey,
                    'Sign': signed_data,
                    'Content-Type': 'application/x-www-form-urlencoded'
            }
            response = requests.post(self.trading_api_url, headers = headers, data = post_data)
            if response.ok:
                trades = json.loads(response.content)
                return trades
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}
    
    def buy(self, currency, rate, amount):
        if self.supported_currency(currency):
            params = {}
            params['command'] = 'buy'
            params['nonce'] = int(time.time()*1000)
            params['currencyPair'] = self.currency_mapping[currency.lower()]
            params['rate'] = rate
            params['amount'] = amount 
            post_data = urllib.parse.urlencode(params).encode('utf8')
            signed_data = hmac.new(self.APISecret, post_data, hashlib.sha512).hexdigest()
            headers = {
                    'Key': self.APIkey,
                    'Sign': signed_data,
                    'Content-Type': 'application/x-www-form-urlencoded'
            }
            response = requests.post(self.trading_api_url, headers = headers, data = post_data)
            if response.ok:
                message = json.loads(response.content)
                return message
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}
    
    def sell(self, currency, rate, amount):
        if self.supported_currency(currency):
            params = {}
            params['command'] = 'sell'
            params['nonce'] = int(time.time()*1000)
            params['currencyPair'] = self.currency_mapping[currency.lower()]
            params['rate'] = rate
            params['amount'] = amount 
            post_data = urllib.parse.urlencode(params).encode('utf8')
            signed_data = hmac.new(self.APISecret, post_data, hashlib.sha512).hexdigest()
            headers = {
                    'Key': self.APIkey,
                    'Sign': signed_data,
                    'Content-Type': 'application/x-www-form-urlencoded'
            }
            response = requests.post(self.trading_api_url, headers = headers, data = post_data)
            if response.ok:
                message = json.loads(response.content)
                return message
            else:
                return {'msg': 'Cannont get data from server'}
        else:
            return {'msg':'Unsupported currency.'}
#methods for websocket ticker streaming that WebSocketApp uses (from websocket module)
#called when receiving message update from server
def on_message(ws, message):
    global last_price
    ticker_update = json.loads(message)
    for data in ticker_update:
        if type(data) is list and data[0] ==121: # id(USDT_BTC) = 121
            json_data = {}
            for i in range(1,len(data)):
                if i == 1:
                    json_data['last'] = data[i]
                elif i == 2:
                    json_data['aks'] = data[i]                   
                elif i==3:
                    json_data['bid'] = data[i]
                elif i==5:
                    json_data['volume'] = data[i]
                elif i==8:
                    json_data['high'] = data[i]
                elif i==9:
                    json_data['low'] = data[i]
            lock.acquire()
            last_price = json_data
            lock.release()

#called when error occures
def on_error(ws, error):
    print(error)

#called on closing websocket
def on_close(ws):
    print("***Closing connection with Poloniex.***")

#called on opening websocket
def on_open(ws):
    print("***Establishing connection to Poloniex.***")
    ws.send(json.dumps({
           'command':'subscribe',
           'channel':"1002"
           }))

#-----TESTING-----#
'''
supported_currencies = ['BTC', 'XRP', 'ETH', 'EOS', 'LTC', 'BCH']
poloniex = Poloniex(
    b'RIQLNWYL-9JAW7LED-RERIXTCG-GMEZILF0',
    b'605047018c0df5e8522879542470c88b2f5ebe7a1faa43eb74f983c98ca23d5fa294324b71f073196572d91c0b7fe414cbf04b01ca8675e5a9956c6bbd125eab', 
    supported_currencies
    )
print(poloniex.sell('BTC', 2000, 0.0006))
'''